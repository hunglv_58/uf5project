package models;

import java.lang.String;
import java.util.ArrayList;
import java.util.*;
import play.mvc.*;
import play.data.validation.Constraints;
import play.db.ebean.Model;

/**
 * Created by HungLe on 20/04/2015.
 */

public class Student extends Model{
	
    @Constraints.Required
    public String stt;
    @Constraints.Required
    public String ten;
    @Constraints.Required
    public String ngaysinh;
    @Constraints.Required
    public String mssv;
    @Constraints.Required
    public String diachi;
    @Constraints.Required
    public String email;
    public String pass; // Chua su dung

    public Student() {}
    public Student(String stt, String ten, String ngaysinh,String mssv, String diachi, String email){
        this.stt = stt;
        this.ten = ten;
        this.ngaysinh = ngaysinh;
        this.mssv = mssv;
        this.diachi = diachi;
        this.email = email;
    }

    private static List<Student> students;
    static {
        students = new ArrayList<Student>();
        students.add(new Student("1", "Lê Văn Hùng", "17/06/1995", "13020200", "Vĩnh Phúc", "hunga1ok@gmail.com"));
        students.add(new Student("2", "Đặng Thanh Linh", "dd/mm/yyyy", "1302xxxx","Thanh Hóa","linhdt_580@vnu.edu.vn"));
        students.add(new Student("3", "Vũ Văn Hậu", "dd/mm/yyyy", "1302xxxx","Vĩnh Phúc","hauvv_58@vnu.edu.vn"));
        students.add(new Student("4", "Nguyễn Việt Dương", "dd/mm/yyyy", "1302xxxx","Vĩnh Phúc","duongnv_58@vnu.edu.vn"));
        students.add(new Student("5", "Nguyễn Đức Anh", "dd/mm/yyyy", "1302xxxx","Vĩnh Phúc","anhnd_58@vnu.edu.vn"));
    }
    public static List<Student> findAll() {
        return new ArrayList<Student>(students);
    }

    public static Student findByEmail(String email) {
        for (Student candicate : students) {
            if(candicate.equals(email)) {
                return candicate;
            }
        }
        return null;
    }

	 public static Student findByMSSV(String mssv) {
        for (Student candicate : students) {
			 if (candicate.equals(mssv)) {
				 return candicate;
			 }
		}
		return null;
    }
	
    public static List<Student> findByName(String term) {
        final List<Student> results = new ArrayList<Student>();
        for(Student candidate : students) {
            if(candidate.ten.toLowerCase().contains(term.toLowerCase())) {
                results.add(candidate);
            }
        }
        return null;
    }

    public static boolean remove(Student student) {
        return student.remove(student);
    }

    public void save() {
        students.remove(findByEmail(this.email));
        students.add(this);
    }

    public String toString(){
        return String.format("  %s  -  MSSV: %s", ten, mssv);
    }


}

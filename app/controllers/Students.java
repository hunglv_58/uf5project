package controllers;

import models.Student;
import  play.mvc.Controller;
import  play.mvc.Result;
import  play.data.Form;

import java.lang.String;
import java.util.ArrayList;
import java.util.List;
import views.html.students.list;
import views.html.*;
import views.html.students.details;

/**
 * Created by HungLe on 19/04/2015.
 */

public class Students extends Controller{
    private static final Form<Student> studentForm = Form.form(Student.class);

    public static Result list() {
        List<Student> students = Student.findAll();
        return ok(list.render(students));
    }

    public static Result newStudent() {
        return ok(details.render(studentForm));
    }

    public static Result details(String stt){
        return TODO;
    }

    public static Result save() {
        Form<Student> boundForm = studentForm.bindFromRequest();
        Student student = boundForm.get();
        student.save();
        return ok(String.format("Saved student %s", student));
    }
	
    public static Result delete(String mssv) {
        final Student student = Student.findByMSSV(mssv);
        if(student == null) {
            return notFound(String.format("Student %s does not exists.",mssv));
        }
        student.delete();
        return redirect(routes.Students.list());
    }
}
